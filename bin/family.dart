abstract class Family {
  var name;
  var occupation;
  var age;
  var pronoun;

  Family(name, occupation, age, pronoun) {
    this.name = name;
    this.occupation = occupation;
    this.age = age;
    this.pronoun = pronoun;
  }
}

class Fam {
  void introduce() {}
  void occupated() {}
  void position() {}
}

class Father extends Family implements Fam {
  Father(super.name, super.occupation, super.age, super.pronoun);

  @override
  void introduce() {
    print("Hello , Dad name is ${name}");
  }

  @override
  void occupated() {
    print("His your occupation is  ${occupation}");
  }

  @override
  void position() {
    print("You can called is ${pronoun}");
  }
}

class Mother extends Family implements Fam {
  Mother(super.name, super.occupation, super.age, super.pronoun);

  @override
  void introduce() {
    print("Hello , Mom name is ${name}");
  }

  @override
  void occupated() {
    print("Her your occupation is  ${occupation}");
  }

  @override
  void position() {
    print("You can called is ${pronoun}");
  }
}

class Sister extends Family implements Fam {
  Sister(super.name, super.occupation, super.age, super.pronoun);

  @override
  void introduce() {
    print("Hello , sis name is ${name}");
  }

  @override
  void occupated() {
    print("Her your occupation is  ${occupation}");
  }

  @override
  void position() {
    print("You can called is ${pronoun}");
  }
}

class Nippon extends Family implements Fam {
  Nippon(super.name, super.occupation, super.age, super.pronoun);

  @override
  void introduce() {
    print("Hello , my name is ${name}");
  }

  @override
  void occupated() {
    print("My occupation is  ${occupation}");
  }

  @override
  void position() {
    print("You can called is ${pronoun}");
  }
}

void main() {
  var Dad = new Father("Tang", "Empty", 41, "Father");
  Dad.introduce();
  Dad.occupated();
  Dad.position();
  print("------------------");

  var Mom = new Mother("Foon", "Business", 39, "Mother");
  Mom.introduce();
  Mom.occupated();
  Mom.position();
  print("------------------");

  var Sis = new Sister("Naple", "Student", 12, "Younger Sister");
  Sis.introduce();
  Sis.occupated();
  Sis.position();
  print("------------------");

  var Me = new Nippon("Nippon", "Student", 21, "Older Sister");
  Me.introduce();
  Me.occupated();
  Me.position();
  print("------------------");
}
